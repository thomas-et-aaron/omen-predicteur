import tkinter as tk
from tkinter import ttk
import csv

def lire_donnees_csv(base_de_donnee):
    """
    la fonction lit les données du fichier CSV et les stockent dans le dictionnaire dico_athletes.
    base_de_donnee est un argument qui represente le chemin vers le fichier CSV contenant les données des athlètes. Il n'y en realité pas de chemin car le fichier est dejà dans le même dossier.
    La fonction renvoie un dictionnaire contenant les données des athlètes.
    """
    dico_athletes = {}
    with open(base_de_donnee, 'r', newline='') as csvfile:
        lecteur_csv = csv.DictReader(csvfile) #permet de lire le fichier csv
        for row in lecteur_csv: #row correspond au rangée donc au attribut du fichier
            nom = row['Prenom']
            age = int(row['age'])
            height = int(row['height'])
            weight = int(row['weight'])
            nb_victoire = int(row['nb_victoire'])
            ratio_V_D = float(row['ratio_V_D'])
            sport = row['sport'].lower()  # Conversion en minuscules
            dernier_chrono_athletisme = float(row['dernier_chrono_athletisme']) if row['dernier_chrono_athletisme'].strip() else 0.0 #strip() permet de supprime les espaces vides.Si c'est vide, on met 0.0 pour la facilité de lecture.
            dernier_chrono_natation = float(row['dernier_chrono_natation']) if row['dernier_chrono_natation'].strip() else '' # On fait pareille mais avec '' si c'est vide
            dernier_chrono_cyclisme = float(row['dernier_chrono_cyclisme']) if row['dernier_chrono_cyclisme'].strip() else ''

            dico_athletes[nom] = {'age': age, 'height': height, 'weight': weight, 'nb_victoire': nb_victoire, 'ratio_V_D': ratio_V_D, 'sport': sport, 'dernier_chrono_athletisme': dernier_chrono_athletisme, 'dernier_chrono_natation': dernier_chrono_natation, 'dernier_chrono_cyclisme': dernier_chrono_cyclisme}
    return dico_athletes

def Tri_age(dico_athletes):
    """
    La fonction trie le dictionnaire `dico_athletes` par l'âge des athlètes en extrayant les paires clé-valeur, 
    les triant par la valeur de l'âge de chaque athlète, puis reconstruisant un nouveau dictionnaire à partir des paires triées, 
    où les athlètes sont classés par ordre croissant d'âge.
    key=lambda définit une fonction anonyme (lambda function) qui spécifie comment les éléments doivent être comparés lors du tri. 
    En l'occurrence, elle indique que le tri doit se faire en fonction de la valeur retournée par cette fonction lambda, 
    qui est ici la valeur de l'âge (`item[1]['age']`) de chaque élément du dictionnaire.

    """
    return dict(sorted(dico_athletes.items(), key=lambda item: item[1]['age']))

# Fonction pour trier par taille
def Tri_height(dico_athletes):
    """
    C'est le même principe qu'avec la fonction Tri_age() mais on utilise `item[1]['height']` à la place de `item[1]['age']'
    """
    return dict(sorted(dico_athletes.items(), key=lambda item: item[1]['height']))

# Fonction pour trier par poids
def Tri_weight(dico_athletes):
    """
    C'est le même principe qu'avec la fonction Tri_age() mais on utilise `item[1]['weight']` à la place de `item[1]['age']'
    """
    return dict(sorted(dico_athletes.items(), key=lambda item: item[1]['weight']))

# Fonction pour trier par nombre de victoires
def Tri_nb_victoire(dico_athletes):
    """
    C'est le même principe qu'avec la fonction Tri_age() mais on utilise `item[1]['nb_victoire']` à la place de `item[1]['age']'
    """
    return dict(sorted(dico_athletes.items(), key=lambda item: item[1]['nb_victoire']))

# Fonction pour trier par ratio Victoire/Défaite
def Tri_ratio_V_D(dico_athletes):
    """
    C'est le même principe qu'avec la fonction Tri_age() mais on utilise `item[1]['ratio_V_D']` à la place de `item[1]['age']'
    """
    return dict(sorted(dico_athletes.items(), key=lambda item: item[1]['ratio_V_D']))

def Tri_natation(dico_athletes):
    """
    La fonction crée un nouveau dictionnaire `athletes_natation` qui filtre les éléments du dictionnaire `dico_athletes` pour ne garder que ceux dont la valeur associée à la clé `'sport'`, 
    soit égale à `'natation'`.
    """
    athletes_natation = {nom: info for nom, info in dico_athletes.items() if info['sport'].lower() == 'natation'}
    return athletes_natation

def Tri_cyclisme(dico_athletes):
    """
    C'est le même principe qu'avec la fonction Tri_natation mais en gardant que ceux dont la valeur associée à la clé `'sport'`, 
    soit égale à `'cyclisme'`.
    """
    athletes_cyclisme = {nom: info for nom, info in dico_athletes.items() if info['sport'].lower() == 'cyclisme'}
    return athletes_cyclisme


# Fonction de tri des athlètes par rapport à un autre athlète spécifique
def tri_athletes(athlete_ref, liste_athletes):
    """
    L'algorithme compare les athlètes du fichier csv à un athlète de réference et attribue des points par rapport aux attributs des athletes.

    """
    scores = []
    for nom, athlete in liste_athletes:
        score = 0
        if 'sport' in athlete and 'sport' in athlete_ref:
            if athlete['sport'].lower() == athlete_ref['sport'].lower():  # Vérifier si le sport est le même
                if athlete['age'] < 30:
                    score += 1
                if athlete['height'] > athlete_ref['height']:
                    score += 1
                if athlete['weight'] < athlete_ref['weight']:
                    score += 1
                if athlete['nb_victoire'] > athlete_ref['nb_victoire']:
                    score += 1
                if athlete['ratio_V_D'] > athlete_ref['ratio_V_D']:
                    score += 1
                if athlete['dernier_chrono_athletisme'] < athlete_ref['dernier_chrono_athletisme']:
                    score += 1
        scores.append((nom, score))

    # ici on trouve que le score maximum
    max_score = max(scores, key=lambda x: x[1])[1]

    # cette ligne inclu tous les athlètes ayant le score maximum
    sorted_athletes = [nom for nom, score in scores if score == max_score]

    return sorted_athletes


# Fonction de tri des athlètes pratiquant la natation
def Tri_natation(dico_athletes):
    """
    Cette fonction trie les athlètes pratiquant la natation en se basant sur un athlete de reference.
    """
    athletes_natation = []
    for nom, info in dico_athletes.items():
        if info['sport'].lower() == 'natation':
            athletes_natation.append((nom, info))  # Stocker le nom et l'information sur l'athlète dans un tuple
    if athletes_natation:
        athletes_natation_sorted = tri_athletes({'age': 26, 'height': 192, 'weight': 80, 'nb_victoire': 14, 'ratio_V_D': 1.4, 'dernier_chrono_athletisme': 23.90}, athletes_natation)
        resultat_tri = {nom: info for nom, info in dico_athletes.items() if nom in athletes_natation_sorted}  #Cette ligne crée un nouveau dictionnaire `resultat_tri` en extrayant les éléments du dictionnaire `dico_athletes` pour lesquels la clé (`nom`) est présente dans la liste `athletes_natation_sorted`. Cela permet de filtrer les athlètes pratiquant la natation selon l'ordre spécifié dans `athletes_natation_sorted`.    
        return resultat_tri
    else:
        print("Aucun athlète pratiquant la natation trouvé.") #cette étape m'a permis de corriger certain bug d'apparition des athletes dans le dictionnaire final resultat_tri
        return {}

# Fonction de tri des athlètes pratiquant le cyclisme
def Tri_cyclisme(dico_athletes):
    """
    Cette fonction trie les athlètes pratiquant le cyclisme en se basant sur un athlete de reference.
    on y retrouve le même fonctionnement que le fonction Tri_natation
    """
    athletes_cyclisme = []
    for nom, info in dico_athletes.items():
        if info['sport'].lower() == 'cyclisme':
            athletes_cyclisme.append((nom, info))
    if athletes_cyclisme:
        athletes_cyclisme_sorted = tri_athletes({'age': 23, 'height': 178, 'weight': 70, 'nb_victoire': 6, 'ratio_V_D': 1.70, 'dernier_chrono_athletisme': 55.433}, athletes_cyclisme)
        resultat_tri = {nom: info for nom, info in dico_athletes.items() if nom in athletes_cyclisme_sorted}      
        return resultat_tri
    else:
        print("Aucun athlète pratiquant le cyclisme trouvé.")
        return {}


def Tri_athletisme(dico_athletes):
    """
    Cette fonction trie les athlètes pratiquant l'athletisme en se basant sur un athlete de reference.
    on y retrouve le même fonctionnement que le fonction Tri_natation
    """
    athletes_athletisme = []

    # On prend les athlètes pratiquant l'athlétisme
    for nom, info in dico_athletes.items():
        if info['sport'].lower() == 'athletisme':
            athletes_athletisme.append((nom, info))

    # On verifie s'il y a des athlètes pratiquant l'athlétisme avant de trier
    if athletes_athletisme:
        athletes_athletisme_sorted = tri_athletes({'age': 26, 'height': 190, 'weight': 76, 'nb_victoire': 4, 'ratio_V_D': 1.09, 'dernier_chrono_athletisme': 48.80}, athletes_athletisme)
        
        # On créer un dictionnaire avec les noms des athlètes comme clés
        resultat_tri = {nom: dico_athletes[nom] for nom in athletes_athletisme_sorted}
        return resultat_tri
    else:
        print("Aucun athlète pratiquant l'athlétisme trouvé.")
        return {}



# Cette fonction affiche les résultats dans une nouvelle fenêtre tout cela grâce à Tkinter
def afficher_resultats(resultat_tri, titre, unite):
    """
    Affiche les résultats dans une nouvelle fenêtre Tkinter.
    resultat_tri est le dictionnaire contenant les données des athlètes triés.
    titre est le titre de la fenêtre.
    unite est l'unité à afficher avec les données (par exemple, "ans", "cm", "kg").
    """
    # On créer notre fenêtre
    result_window = tk.Toplevel(root)
    result_window.title(titre)

    # On créer un widget Treeview pour afficher les résultats
    result_tree = ttk.Treeview(result_window, columns=("Name", "Age", "Height", "Weight", "Victoires", "Ratio V/D", "Sport", "Dernier chrono athlétisme", "Dernier chrono natation", "Dernier chrono cyclisme"), show="headings")
    result_tree.heading("Name", text="Name")
    result_tree.heading("Age", text="Age")
    result_tree.heading("Height", text="Height")
    result_tree.heading("Weight", text="Weight")
    result_tree.heading("Victoires", text="Victoires")
    result_tree.heading("Ratio V/D", text="Ratio V/D")
    result_tree.heading("Sport", text="Sport")
    result_tree.heading("Dernier chrono athlétisme", text="Dernier chrono athlétisme")
    result_tree.heading("Dernier chrono natation", text="Dernier chrono natation")
    result_tree.heading("Dernier chrono cyclisme", text="Dernier chrono cyclisme")
    result_tree.pack(padx=20, pady=20)

    # On affcihe les résultats dans le widget Treeview
    for nom, info in resultat_tri.items():
        dernier_chrono_athletisme = info.get('dernier_chrono_athletisme', '')
        dernier_chrono_natation = info.get('dernier_chrono_natation', 'N/A')  # Je remplace Les valeurs vides par "N/A" qui signifie en anglais no answer donc pas de réponse
        dernier_chrono_cyclisme = info.get('dernier_chrono_cyclisme', 'N/A')  # On fait pareille ici 

        result_tree.insert("", "end", values=(nom, info['age'], info['height'], info['weight'], info['nb_victoire'], info['ratio_V_D'], info['sport'], dernier_chrono_athletisme, dernier_chrono_natation, dernier_chrono_cyclisme))








# Cette fonction correspond à la fonction à exécuter lorsque le bouton pour le tri par âge est cliqué 
def on_button_click_age():
    """
    Fonction à exécuter lorsque le bouton est cliqué pour le tri par âge.
    """
    base_de_donnee = "base_de_donnée.csv"
    dico_athletes = lire_donnees_csv(base_de_donnee)
    resultat_tri = Tri_age(dico_athletes)
    afficher_resultats(resultat_tri, "age", "ans")

# Cette fonction correspond à la fonction à exécuter lorsque le bouton pour le tri par taille est cliqué 
def on_button_click_height():
    """
    Fonction à exécuter lorsque le bouton est cliqué pour le tri par height(taille).
    """
    base_de_donnee = "base_de_donnée.csv"
    dico_athletes = lire_donnees_csv(base_de_donnee)
    resultat_tri = Tri_height(dico_athletes)
    afficher_resultats(resultat_tri, "height", "cm")

# Cette fonction correspond à la fonction à exécuter lorsque le bouton pour le tri par poid est cliqué 
def on_button_click_weight():
    """
    Fonction à exécuter lorsque le bouton est cliqué pour le tri par weight(poid).
    """
    base_de_donnee = "base_de_donnée.csv"
    dico_athletes = lire_donnees_csv(base_de_donnee)
    resultat_tri = Tri_weight(dico_athletes)
    afficher_resultats(resultat_tri, "weight", "kg")

# Cette fonction correspond à la fonction à exécuter lorsque le bouton pour le tri par nombre de victoire est cliqué 
def on_button_click_nb_victoire():
    """
    Fonction à exécuter lorsque le bouton est cliqué pour le tri nb_victoire.
    """
    base_de_donnee = "base_de_donnée.csv"
    dico_athletes = lire_donnees_csv(base_de_donnee)
    resultat_tri = Tri_nb_victoire(dico_athletes)
    afficher_resultats(resultat_tri, "nb_victoire", "victoire(s)")

# Cette fonction correspond à la fonction à exécuter lorsque le bouton pour le tri par ratio victoire/defaite est cliqué 
def on_button_click_ratio_V_D():
    """
    Fonction à exécuter lorsque le bouton est cliqué pour le tri ratio_V_D.
    """
    base_de_donnee = "base_de_donnée.csv"
    dico_athletes = lire_donnees_csv(base_de_donnee)
    resultat_tri = Tri_ratio_V_D(dico_athletes)
    afficher_resultats(resultat_tri, "ratio_V_D", "de ratio")

# Cette fonction correspond à la fonction à exécuter lorsque le bouton pour le tri des pratiquants d'athletisme est cliqué 
def on_button_click_athletisme():
    """
    Fonction à exécuter lorsque le bouton est cliqué pour le tri athletisme.
    """
    base_de_donnee = "base_de_donnée.csv"
    dico_athletes = lire_donnees_csv(base_de_donnee)
    resultat_tri = Tri_athletisme(dico_athletes)
    afficher_resultats(resultat_tri, "athletisme", "")


# Cette fonction correspond à la fonction à exécuter lorsque le bouton pour le tri des pratiquants de natation est cliqué 
def on_button_click_natation():
    """
    Fonction à exécuter lorsque le bouton est cliqué pour le tri natation.
    """
    base_de_donnee = "base_de_donnée.csv"
    dico_athletes = lire_donnees_csv(base_de_donnee)
    resultat_tri = Tri_natation(dico_athletes)
    afficher_resultats(resultat_tri, "natation", "")

# Cette fonction correspond à la fonction à exécuter lorsque le bouton pour le tri des pratiquants de cyclisme est cliqué 
def on_button_click_cyclisme():
    """
    Fonction à exécuter lorsque le bouton est cliqué pour le tri cyclisme.
    """
    base_de_donnee = "base_de_donnée.csv"
    dico_athletes = lire_donnees_csv(base_de_donnee)
    resultat_tri = Tri_cyclisme(dico_athletes)
    afficher_resultats(resultat_tri, "cyclisme", "")
    

# On créer la fenêtre principale
root = tk.Tk()
root.title("Système de Prédiction")

# On defnit un petit style pour les widgets
style = ttk.Style()
style.configure("TButton", font=("Arial", 12), padding=10)
style.configure("TLabel", font=("Arial", 12), padding=10)

# On change la couleur bien évidemment
root.configure(bg="#001f3f")
style.configure("TFrame", background="#001f3f")

# On change la couleur de fond des boutons aussi 
style.map("TButton", background=[('active', '#0074D9'), ('disabled', '#7FDBFF')])

# On créer le cadre pour le texte "Système de Prédiction du gagnant"
label_title = ttk.Label(root, text="Système de Prédiction du gagnant", foreground="white", background="#001f3f")
label_title.pack(pady=20)

# On créer le cadre pour le texte "Algorithmes de tri précis" et les boutons de tri
frame_left = ttk.Frame(root)
frame_left.pack(side="left", padx=20, pady=20)

label_algorithmes = ttk.Label(frame_left, text="Algorithmes de tri précis", foreground="white", background="#001f3f")
label_algorithmes.grid(row=0, column=0, padx=10, pady=10, sticky="w")

# On créer leS Boutons de tri
button_age = ttk.Button(frame_left, text="Trier tous les athlètes en fonction de leur âge", command=on_button_click_age)
button_age.grid(row=1, column=0, padx=10, pady=5, sticky="w")

button_height = ttk.Button(frame_left, text="Trier tous les athlètes en fonction de leur taille", command=on_button_click_height)
button_height.grid(row=2, column=0, padx=10, pady=5, sticky="w")

button_weight = ttk.Button(frame_left, text="Trier tous les athlètes en fonction de leur poids", command=on_button_click_weight)
button_weight.grid(row=3, column=0, padx=10, pady=5, sticky="w")

button_nb_victoire = ttk.Button(frame_left, text="Trier tous les athlètes en fonction de leur nombre de victoires", command=on_button_click_nb_victoire)
button_nb_victoire.grid(row=4, column=0, padx=10, pady=5, sticky="w")

button_ratio_V_D = ttk.Button(frame_left, text="Trier tous les athlètes en fonction de leur ratio de victoire/défaites", command=on_button_click_ratio_V_D)
button_ratio_V_D.grid(row=5, column=0, padx=10, pady=5, sticky="w")

# On créer le cadre pour le texte "Algorithme de Prédiction par sport" et les boutons de tri par sport
frame_right = ttk.Frame(root)
frame_right.pack(side="right", padx=20, pady=20)

label_prediction_sport = ttk.Label(frame_right, text="Algorithme de Prédiction par sport", foreground="white", background="#001f3f")
label_prediction_sport.grid(row=0, column=0, padx=10, pady=10, sticky="e")

# On créer leS Boutons de tri par sport
button_natation = ttk.Button(frame_right, text="Confectionner ma liste prédictive des athlètes gagnants pratiquant la natation", command=on_button_click_natation)
button_natation.grid(row=1, column=0, padx=10, pady=5, sticky="e")

button_cyclisme = ttk.Button(frame_right, text="Confectionner ma liste prédictive des athlètes gagnants pratiquant le cyclisme", command=on_button_click_cyclisme)
button_cyclisme.grid(row=2, column=0, padx=10, pady=5, sticky="e")

button_athletisme = ttk.Button(frame_right, text="Confectionner ma liste prédictive des athlètes gagnants pratiquant l'athlétisme", command=on_button_click_athletisme)
button_athletisme.grid(row=3, column=0, padx=10, pady=5, sticky="e")
#La ligne root.mainloop() est une méthode qui lance la boucle principale de l'interface graphique tkinter. 
#Cette boucle reste active tant que la fenêtre principale (root) est ouverte, permettant ainsi à l'interface utilisateur d'interagir avec les widgets 
#et de répondre aux événements tels que les clics de souris et les pressions de touche.
root.mainloop()