# Omen-Prédicteur

# Guide d'utilisation

## Prérequis

- **Python :** Assurez-vous d'avoir Python installé sur votre machine.
- **Tkinter :** La bibliothèque Tkinter doit être installée. Tkinter est généralement inclus avec Python, mais si ce n'est pas le cas, vous pouvez l'installer via votre gestionnaire de paquets.
- **Organisation des fichiers :** Placez le fichier Python et le fichier CSV dans le même dossier. Ils doivent être localisés ensemble pour que le programme fonctionne correctement.

## Fonctionnement

1. **Inscription des athlètes :** Commencez par inscrire les différents athlètes avec leurs caractéristiques dans la base de données. Ceci est essentiel pour que le programme puisse fonctionner correctement.
2. **Exécution du programme :** Une fois les athlètes inscrits, lancez le programme.
3. **Utilisation de l'interface graphique :** 
   - Après le lancement, l'interface graphique du programme s'ouvrira.
   - Pour obtenir le tableau des athlètes avec leurs caractéristiques, cliquez sur le bouton prévu à cet effet dans l'interface.

Suivez ces étapes pour assurer le bon fonctionnement du programme.